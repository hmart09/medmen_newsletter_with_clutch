﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;

namespace HavenAgency.Modules.NewsLetterWithClutch
{
    public class ClutchNet
    {



        /*
       *will add these values to config file 
       * and pull values from there.
      */

        // Set credentials and variables
        public string urlStr = "https://api-stage.clutch.com";
        public string urlAftrPortStr = "/merchant/";
        public string portStr = "443";


        public string keyStr = "cbe820af-1d44-4d70-a86e-e7943b8b3bea";
        public string secretStr = "TCRVFLJKP6MO78AAGPC7";
        public string locationStr = "MEDMECOM1";
        public string terminalStr = "MEDMECOM1WS";
        public string brandStr = "Medm6152";

        // properties
        public string m_serviceStr = "search";
        public string m_cardNumberStr;
        public string m_jsonStr;
        public string m_email;

        // service default is search
        public string serviceStr
        {
            get
            {
                return this.m_serviceStr;
            }
            set
            {
                this.m_serviceStr = value;

            }

        }

        public string cardNumberStr
        {

            get
            {
                return this.m_cardNumberStr;

            }
            set
            {
                this.m_cardNumberStr = value;
            }

        }

        public string jsonStr
        {

            get
            {
                return this.m_jsonStr;

            }
            set
            {
                this.m_jsonStr = value;

            }

        }

        public string email
        {

            get
            {
                return this.m_email;

            }
            set
            {
                this.m_email = value;

            }

        }

        public bool UpdateCustomerOptions(string email)
        {
            bool flg = false;

            try
            {
                ClutchCard card = ClutchSearch(email);

                //string cNumber = card.Cards[0].cardNumber;
                bool successflg = Convert.ToBoolean(card.success);

                if (successflg && card.Cards.Count > 0)
                {
                    // found existing user
                    // Call ClutchSetOption
                    this.cardNumberStr = card.Cards[0].cardNumber;

                    bool settings = SetUpdateValues();

                    ClutchCard UpdateResponse = CallClutch();

                    if (UpdateResponse.success == "true")
                    {
                        //successfully updated customer card
                        flg = true;

                    }

                }
                else
                {

                    // create new customer
                    // Call clutchCreate Customer
                    // returns card number
                    // call ClutchSetOptions

                    this.jsonStr = "{cardSetId : MedMenCustomers}";
                    this.serviceStr = "allocate";

                    // use returned card number
                    ClutchCard CreateResponse = CallClutch();

                    //Update card Number
                    // check for null
                    if (CreateResponse.Cards.Count > 0)
                    {
                        this.cardNumberStr = CreateResponse.Cards[0].cardNumber;

                    }
                    else { return false; }


                    // need to set card number value
                    // prior to this call
                    bool emailUpdate = UpdateEmail();

                    bool settings = SetUpdateValues();

                    ClutchCard UpdateResponse = CallClutch();

                    if (UpdateResponse.success == "true")
                    {
                        //successfully updated customer card
                        flg = true;
                    }

                }

            }
            catch (Exception)
            {
                return false;

            }



            return flg;

        }

        public bool UpdateEmail()
        {

            bool flg = false;

            this.serviceStr = "updateAccount";

            PrimaryCustomer xprimary = new PrimaryCustomer
            {
                email = this.email

            };

            List<PrimaryCustomer> x = new List<PrimaryCustomer>();
            x.Add(xprimary);


            UpdateEmail updateEmail = new UpdateEmail
            {
                cardNumber = this.cardNumberStr,
                primaryCustomer = x

            };

            this.jsonStr = JsonConvert.SerializeObject(updateEmail, Formatting.Indented);

            this.jsonStr = this.jsonStr.Replace("[", "").Replace("]", "");

            ClutchCard emailUpdateResponse = CallClutch();

            return flg;
        }

        public bool SetUpdateValues()
        {
            bool flg = false;

            this.serviceStr = "updateAccount";

            ThirdPartyUpdates xthirdPartyUpdates = new ThirdPartyUpdates
            {
                thirdParty = "email",
                optIn = "true"

            };


            List<ThirdPartyUpdates> z = new List<ThirdPartyUpdates>();
            z.Add(xthirdPartyUpdates);


            UpdateCard updateCard = new UpdateCard
            {
                cardNumber = this.cardNumberStr,

                thirdpartyupdates = z,

            };

            this.jsonStr = JsonConvert.SerializeObject(updateCard, Formatting.Indented);


            return flg;
        }

        public ClutchCard ClutchSearch(string email)
        {

            this.email = email;

            // Create JSON string
            // pull out and place in specific functions
            this.jsonStr = "{filters:"
                    + "{email:\"" + this.email + "\"}"
                + "}";



            ClutchCard card = CallClutch();

            return card;

        }

        // change to CallClutchApi
        public ClutchCard CallClutch()
        {

            string responseStr;

            // Create full URL
            string fullUrl = urlStr + ":" + portStr + urlAftrPortStr + this.serviceStr;

            // Encode basic auth string
            string auth = Convert.ToBase64String(Encoding.ASCII.GetBytes(keyStr + ":" + secretStr));

            // call will fail if protocol is not specified
            // newer version 4.6 + not needed.
            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;

            // Setup web service call
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(fullUrl);
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Accept = "*/*";
            httpWebRequest.Method = "POST";
            httpWebRequest.Headers.Add("Authorization", "Basic " + auth);
            httpWebRequest.Headers.Add("location", locationStr);
            httpWebRequest.Headers.Add("terminal", terminalStr);
            httpWebRequest.Headers.Add("brand", brandStr);
            httpWebRequest.ContentLength = this.jsonStr.Length;

            // Send JSON string request
            Stream dataStream = httpWebRequest.GetRequestStream();
            dataStream.Write(Encoding.ASCII.GetBytes(this.jsonStr), 0, this.jsonStr.Length);

            // Get response
            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();



            // Read response string
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {

                responseStr = streamReader.ReadToEnd();

            }

            // get values in format that can be referenced
            // uses two classes for nested json values
            // different response for newly created cards

            ClutchCard card = JsonConvert.DeserializeObject<ClutchCard>(responseStr);
            NewCreatedClutchCard nCard = JsonConvert.DeserializeObject<NewCreatedClutchCard>(responseStr);


            if (this.serviceStr == "allocate")
            {
                // create new card
                Cards addCard = new Cards
                {
                    cardNumber = nCard.cardNumber,
                    cardSetId = "MedMenCustomers"

                };

                List<Cards> xx = new List<Cards>();
                xx.Add(addCard);

                ClutchCard allocateCard = new ClutchCard
                {
                    Cards = xx,
                    success = card.success,
                    requestRef = card.requestRef,
                    usedCache = card.usedCache

                };

                string str = allocateCard.Cards[0].cardNumber;

                return allocateCard;

            }



            // Used for testing and debugging
            // Display response
            //System.Console.WriteLine("\r\nRaw JSON Response:\r\n" + auth + "\r\n");
            //System.Console.WriteLine(responseStr);



            return card;

        }
    }

    // sample json returned:
    // {"cards":[{"cardNumber":"2272412670175721","cardSetId":"MedMenCustomers"}]
    // ,"usedCache":false,"success":true,"requestRef":"26b48aa6-a560-45c5-85fc-205864a0c537"}


    public class Cards
    {
        public string cardNumber { get; set; }
        public string cardSetId { get; set; }


    }

    public class ClutchCard
    {
        public List<Cards> Cards { get; set; }
        public string usedCache { get; set; }
        public string success { get; set; }
        public string requestRef { get; set; }

    }

    public class NewCreatedClutchCard
    {
        public string cardNumber { get; set; }
        public string usedCache { get; set; }
        public string success { get; set; }
        public string requestRef { get; set; }

    }


    public class UpdateCard
    {
        public String cardNumber { get; set; }

        public List<ThirdPartyUpdates> thirdpartyupdates { get; set; }



    }

    public class UpdateEmail
    {
        public String cardNumber { get; set; }

        public List<PrimaryCustomer> primaryCustomer { get; set; }

    }

    public class PrimaryCustomer
    {
        public String email { get; set; }

    }

    public class ThirdPartyUpdates
    {
        public string thirdParty { get; set; }
        public string optIn { get; set; }

    }

    public class CreateCard
    {
        public string cardSetId { get; set; }


    }



}