﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="View.ascx.cs" Inherits="HavenAgency.Modules.NewsLetterWithClutch.View" %>

<%--<link rel="stylesheet" data-enableoptimizations="true" href="@App.Path/Portals/_default/Skins/MedmenCorporate_2018/css/global.css" />--%>

<%--<script>
    $(document).ready(function () {
        $("#dnn_ContentPane").addClass("o-section c-theme-grey-light");
    });
</script>--%>


    <header class="o-container u-text-center wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;">
        <h3 class="c-heading-38 u-color-red">Keep in Touch</h3>
        <p class="c-heading-24 u-spacing-40">Stay in the loop on happenings in our stores, new product releases, and exclusive offers.</p>
    </header>

    <div class="wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;">
        <div class="js-newsletter">
            <p class="js-newsletter__success u-text-center" style="display: none;">Thank you for signing up!</p>
            <div class="c-signup js-newsletter__form">
                <div class="c-signup__form o-container">




                    <asp:TextBox class="c-signup__field js-newsletter__email" placeholder="Email Address" ID="txtbxEmail" runat="server"></asp:TextBox>
                    <asp:Button class="c-signup__submit" ID="btnSubmit" runat="server" Text="Submit" Font-Size="Small" OnClick="btnSubmit_Click" />





                </div>

                <asp:Label class="js-newsletter__error u-text-center" Style="display: none; text-transform: uppercase; font-size: 80%; color: #960709" ID="lblResults" runat="server" Text=""></asp:Label>
                <asp:RequiredFieldValidator class="js-newsletter__error u-text-center" Style="display: none; text-transform: uppercase; font-size: 80%; color: #960709" ID="rfEmail" runat="server" ErrorMessage="Your email address is required." Display="Dynamic" ControlToValidate="txtbxEmail"></asp:RequiredFieldValidator>
                <p class="c-signup__disclaimer">
                    By signing up, you consent to our storing your email address and receiving periodic email updates, and you agree to our
                  &nbsp;<a href="/terms-use">Terms of Service</a>&nbsp;
                  and
                  &nbsp;<a href="/privacy-policy">Privacy Policy.</a>
                </p>
            </div>
        </div>
    </div>


